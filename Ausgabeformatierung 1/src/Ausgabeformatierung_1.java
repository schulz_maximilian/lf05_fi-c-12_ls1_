
public class Ausgabeformatierung_1 {

	public static void main(String[] args) {
		System.out.print("Das ist ein Beispielsatz. ");
		System.out.print("Ein Beispielsatz ist das.");
		System.out.print("\nDas ist ein Beispielsatz.\n");
		System.out.print("Ein Beispielsatz ist das.\n");
		String Bsp = "Beispielsatz";
		System.out.print("Das ist ein " + Bsp + ". Ein " + Bsp + " ist das.");
//Print allein gibt das Ergebnis direkt in der Konsole aus, println setzt nach dem ausgegeben einen Zeilenumbruch
		System.out.println("\n      *\n     ***\n    ******\n   ********\n  **********\n *************\n***************\n     ***\n     ***\n");
		
		double d = 22.4234234;
		double a = 111.2222;
		double b = 4.0;
		double c = 1000000.551;
		double e = 97.34;

		System.out.printf( "%.2f\n" , d);
		System.out.printf( "%.2f\n" , a);
		System.out.printf( "%.2f\n" , b);
		System.out.printf( "%.2f\n" , c);
		System.out.printf( "%.2f\n" , e);
	}

}
