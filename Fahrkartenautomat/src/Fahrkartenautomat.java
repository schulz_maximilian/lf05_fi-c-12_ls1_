﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args) {
    while (true)  {
       double zuZahlenderBetrag; 
       double rückgabebetrag;
       
       zuZahlenderBetrag = fahrkartenbestellungErfassen();     
       // Geldeinwurf
       // -----------
       rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben();
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rueckgeldAusgeben(rückgabebetrag);
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
    }
    public static double fahrkartenbestellungErfassen() {
    	Scanner sn = new Scanner(System.in);
    	double zuZahlenderBetrag;
    	double anzahlTickets;
    	
    	System.out.print("Zu zahlender Betrag (EURO): ");
        zuZahlenderBetrag = sn.nextDouble();
        
        System.out.print("Wie viele Tickets wollen Sie?");
        anzahlTickets = sn.nextInt();
        	if (anzahlTickets>10 || anzahlTickets<1) {
        	System.out.println("Ungültige eingabe. 10 Tickets maximal möglich");
        	anzahlTickets = 1;
        	
        }
        zuZahlenderBetrag *= anzahlTickets;
        return zuZahlenderBetrag;
    	
    }
    
    public static double fahrkartenBezahlen(double zuZahlen) {
    	Scanner scn2 = new Scanner(System.in);
    	double rückgabeBetrag;
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneMünze;
    	
    	
    	while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: %.2f €\n", (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = scn2.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	
    	rückgabeBetrag = eingezahlterGesamtbetrag - zuZahlen;
    	
    	return rückgabeBetrag;
    	
    }

    public static void fahrkartenAusgeben() {
    	 System.out.println("\nFahrschein wird ausgegeben");
         for (int i = 0; i < 8; i++)
         {
            System.out.print("=");
            try {
  			Thread.sleep(250);
  		} catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
         }
         System.out.println("\n\n");    	
    }

    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }
    }
}