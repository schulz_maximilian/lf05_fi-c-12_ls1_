
/**
 * @author Schulz Maximilian
 *
 */
public class Main {

	public static void main(String[] args) {
		// Objekt Raumschiff initialisieren
		Raumschiff klingonen = new Raumschiff();
		Raumschiff romulaner = new Raumschiff();
		Raumschiff vulkanier = new Raumschiff();

		// Objekt Ladung Initialisieren
		Ladung l1 = new Ladung();
		Ladung l2 = new Ladung();
		Ladung l3 = new Ladung();
		Ladung l4 = new Ladung();
		Ladung l5 = new Ladung();
		Ladung l6 = new Ladung();
		Ladung l7 = new Ladung();

		// den Ladungsobjekten die Atributte setzen
		l1.setMenge(200);
		l1.setBezeichnung("Ferengi Schneckensaft");
		l2.setMenge(200);
		l2.setBezeichnung("Bat�lezh Klingonen Schwert");
		l3.setMenge(5);
		l3.setBezeichnung("Borg Schrott");
		l4.setMenge(2);
		l4.setBezeichnung("Rote Materie");
		l5.setMenge(50);
		l5.setBezeichnung("Plasma-Waffe");
		l6.setMenge(35);
		l6.setBezeichnung("Forschungsonde");
		l7.setMenge(3);
		l7.setBezeichnung("Photonentorpedo");

		// Klingonen Raumschiff Anfangszustand setzen
		klingonen.setPhotonentorpedoAnzahl(1);
		klingonen.setEnergieversorgungInProzent(100);
		klingonen.setSchildInProzent(100);
		klingonen.setHuelleInProzent(100);
		klingonen.setLebenserhaltungssystemInProzent(100);
		klingonen.setSchiffsname("IKS Hegh�ta");
		klingonen.setAndroidenAnzahl(2);
		klingonen.addLadung(l1);
		klingonen.addLadung(l2);

		// Romulaner Raumschiff Anfangszustand setzen
		romulaner.setPhotonentorpedoAnzahl(2);
		romulaner.setEnergieversorgungInProzent(100);
		romulaner.setSchildInProzent(100);
		romulaner.setHuelleInProzent(100);
		romulaner.setLebenserhaltungssystemInProzent(100);
		romulaner.setSchiffsname("IRW Khazara");
		romulaner.setAndroidenAnzahl(2);
		romulaner.addLadung(l3);
		romulaner.addLadung(l4);
		romulaner.addLadung(l5);

		// Vulkanier Raumschiff Anfangszustand setzen
		vulkanier.setPhotonentorpedoAnzahl(0);
		vulkanier.setEnergieversorgungInProzent(80);
		vulkanier.setSchildInProzent(80);
		vulkanier.setHuelleInProzent(50);
		vulkanier.setLebenserhaltungssystemInProzent(100);
		vulkanier.setSchiffsname("Ni�Var");
		vulkanier.setAndroidenAnzahl(5);
		vulkanier.addLadung(l6);
		vulkanier.addLadung(l7);

		// Zum Testen
		klingonen.photonentorpedoSchiessen(romulaner);
		romulaner.phaserkanoneSchiessen(klingonen);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		vulkanier.reperaturDurchfuehren(true, true, true, 100);
		vulkanier.photonentorpedosLaden(100);
		vulkanier.ladungsverzeichnisAufräumen();
		for (int i = 0; i < 2; i++) {
			klingonen.photonentorpedoSchiessen(romulaner);
		}

		// Zustandsausgabe aller Schiffe
		System.out.println("Zustand der Schiffe");
		System.out.println("_____________________");
		System.out.println(klingonen.getSchiffsname());
		klingonen.zustandRaumschiff();
		System.out.println("~~~~~~~~~~~~~");
		System.out.println(romulaner.getSchiffsname());
		romulaner.zustandRaumschiff();
		System.out.println("~~~~~~~~~~~~~");
		System.out.println(vulkanier.getSchiffsname());
		vulkanier.zustandRaumschiff();
		System.out.println("---------------------");

		// Ladungsverzeichnisausgabe
		System.out.println("Ladungsverzeichnis der Schiffe");
		System.out.println("_____________________");
		System.out.println(klingonen.getSchiffsname());
		klingonen.ladungsverzeichnisAusgeben();
		System.out.println("~~~~~~~~~~~~~");
		System.out.println(romulaner.getSchiffsname());
		romulaner.ladungsverzeichnisAusgeben();
		System.out.println("~~~~~~~~~~~~~");
		System.out.println(vulkanier.getSchiffsname());
		vulkanier.ladungsverzeichnisAusgeben();
		System.out.println("_____________________");
		System.out.println(" ");

		System.out.println(vulkanier.eintraegeLogbuchZurueckGeben() +""+ klingonen.eintraegeLogbuchZurueckGeben()
				+ romulaner.eintraegeLogbuchZurueckGeben());
	}
}
